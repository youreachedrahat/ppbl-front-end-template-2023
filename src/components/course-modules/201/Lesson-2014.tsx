import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module201.json";
import Docs2014 from "@/src/components/course-modules/201/Docs2014.mdx";

export default function Lesson2014() {
  const slug = "2014";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={201} sltId="201.4" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2014 />
    </LessonLayout>
  );
}
